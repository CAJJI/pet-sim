﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PetSequence
{
    public List<PetAction> steps = new List<PetAction>();    
}

public class PetSequenceBehaviour {

    public Pet pet;
    public PetSequence sequence = new PetSequence();    
    List<PetAction> steps = new List<PetAction>();

    public void Initialize(Pet pet)
    {
        this.pet = pet;
    }

    public bool PlaySequence()
    {
        foreach(PetAction step in sequence.steps)
        {
            if (!step.complete)
            {
                Debug.Log(step);
                step.Play();
                return true;
            }            
        }
        Debug.Log("complete");
        Clear();
        return false;
    }

    public void StartSequence(PetSequence newSequence)
    {
        sequence = newSequence;
        Debug.Log("set sequence");
    }

    public void Clear()
    {
        sequence = new PetSequence();        
        steps = new List<PetAction>();
        sequence.steps = steps;
    }

    public virtual PetSequence GetFood(NeedSatisfier foodSource)
    {        
        steps.Add(PetSequenceStep.MoveToPosition(pet, foodSource.transform.position, foodSource.engageDistance, PetSpeedType.Slow));
        steps.Add(PetSequenceStep.FacePosition(pet, foodSource.transform.position));
        steps.Add(PetSequenceStep.PlayAnimation(pet, PetAnimationType.Eat));
        steps.Add(PetSequenceStep.EatFromFoodSource(pet, foodSource));
        steps.Add(PetSequenceStep.WaitForAnimationEvent(pet, PetAnimationEventType.Finish));        
        return sequence;
    }    

    public PetSequence GoToBed(NeedSatisfier bed)
    {
        steps.Add(PetSequenceStep.MoveToPosition(pet, bed.transform.position, 0, PetSpeedType.Slow));
        steps.Add(PetSequenceStep.FacePosition(pet, bed.transform.position + Vector3.forward));
        GoToSleep();
        return sequence;
    }

    public virtual PetSequence GoToSleep()
    {
        steps.Add(PetSequenceStep.PlayAnimation(pet, PetAnimationType.Sleep));
        steps.Add(PetSequenceStep.WaitForAnimationEvent(pet, PetAnimationEventType.Finish));
        return sequence;
    }
}
