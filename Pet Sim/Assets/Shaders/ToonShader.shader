﻿Shader "Custom/ToonShader"
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_LightTex("Texture", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_Threshold("Cel Threshold", Range(1., 100.)) = 5.
		_AmbientColor("Ambient Color", Color) = (1,1,1,1)
		_AmbientIntensity("Ambient Intensity", Range(0., 10)) = 0.1
		_BackLightColor("Back Light Color", Color) = (1,1,1,1)
		_BackLightIntensity("Back Light Intensity", Range(0, 1)) = 0.1
		_BackLightStrength("Back Light Strength", Range(0,1)) = 0.1
			_Specularity("Specularity", Range(0.,10.)) = 0
		_IlluminationColor("Illumination Color", Color) = (0,0,0,1)
			_IlluminationOverride("Illumination Override", Range(0.,1.)) = 0
		_Illumination("Illumination", Range(0.,10.)) = 1
		_NormalIllumination("Normal Illumination", Range(0.,1.)) = 1
	}
		SubShader
	{
		Tags{"RenderType" = "Transparent" "LightMode" = "ForwardBase" }
		
		Blend SrcAlpha OneMinusSrcAlpha

		Pass
	{
		CGPROGRAM

#pragma vertex vert
#pragma fragment frag
#pragma multi_compile_fwdbase

#include "AutoLight.cginc"
#include "UnityCG.cginc"
		

		struct v2f
	{		
		float4 pos : SV_POSITION;		
		float2 uv : TEXCOORD0;
		float3 worldPos : TEXCOORD1;
		float3 worldNormal : NORMAL;		
		LIGHTING_COORDS(1,2)
	};

	float _Threshold;		

	float LightToonShading(float3 normal, float3 lightDir, float atten)
	{
		float NdotL = max(0.0, dot(normalize(normal), normalize(lightDir)));
		//float shading = floor(NdotL * _Threshold) / (_Threshold - 0.5);
		float shading = floor(NdotL * _Threshold) / (_Threshold - 0.5);

		half diff = NdotL * 0.5 + 0.5;				
		shading *= diff * (atten);

		return shading;
	}

	float _BackLightIntensity;

	float BackLighting(float3 normal, float3 lightDir) 
	{
		float NdotL = max(0.0, dot(normalize(normal), normalize(lightDir)));
		float lighting = max(0, _BackLightIntensity - NdotL);
		return lighting;
	}

	uniform int globalLightCount;	
	uniform float4 globalLightPosition[20];
	uniform float4 globalLightColor[20];
	uniform float globalLightIntensity[20];
	uniform float globalLightRadius[20];
	uniform float globalLightDampen[20];

	float _NormalIllumination;
	float _Illumination;
	fixed4 _IlluminationColor;
	float _IlluminationOverride;
	float _Specularity;

	fixed4 LightSource(float3 position, float3 normal, fixed4 tex, float3 viewDir) 
	{		
		fixed4 col = (0, 0, 0, 0);
		int lightCount = min(4,globalLightCount);
		for (int i = 0; i < lightCount; i++) {
			float NdotL = max(0.0, dot(normalize(normal), normalize(globalLightPosition[i] - position)));
			float shading = floor(NdotL * _Threshold) / (_Threshold - 0.5);			
			float norm = saturate(shading + (1 - _NormalIllumination));
			float radius = globalLightRadius[i];
			float intensity = globalLightIntensity[i];
			float damp = globalLightDampen[i];			
			float4 dis = distance(globalLightPosition[i], position);						
			dis = min((1/damp), (max(0.1,(1 / dis)*radius)-0.1));			
			//dis -= min(0,(position.y - globalLightPosition[i].y)*dis);
			float4 lightColor = globalLightColor[i] * (1-_IlluminationOverride);			
			col += tex * ((_IlluminationColor + lightColor) * dis * norm * _Illumination * intensity);			
			float specDot = max(0,dot(normal, normalize(globalLightPosition[i]- position) * (_Specularity)));
			float3 specCol = (1, 1, 1);
			specCol += (_IlluminationColor * _IlluminationOverride);
			col.rgb += specCol * saturate(pow(max(0.0, dot(reflect(normalize(position - globalLightPosition[i]), normal), viewDir)), 1) * specDot);
		}
		return col;
	}

	sampler2D _LightTex;
	float4 _LightTex_ST;
	sampler2D _MainTex;
	float4 _MainTex_ST;
	fixed4 _Color;

	v2f vert(appdata_full v)
	{					
		v2f o;						
		o.pos = UnityObjectToClipPos(v.vertex);
		o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);		
		o.worldPos = mul(unity_ObjectToWorld, v.vertex);
		o.worldNormal = mul(v.normal.xyz, (float3x3) unity_WorldToObject);		
		TRANSFER_VERTEX_TO_FRAGMENT(o);
		return o;
	}

	fixed4 _LightColor0;	
	fixed4 _AmbientColor;
	fixed4 _BackLightColor;	
	half _AmbientIntensity;
	half _BackLightStrength;

	fixed4 frag(v2f i) : SV_Target
	{						
		float atten = min(1, LIGHT_ATTENUATION(i)*10);		
		fixed4 col = tex2D(_MainTex, i.uv) * _Color;
		fixed4 litTex = tex2D(_LightTex, i.uv);
		
		fixed4 toonLight = _AmbientColor * LightToonShading(i.worldNormal, _WorldSpaceLightPos0.xyz, atten) * _AmbientIntensity;

		float backLightValue = BackLighting(i.worldNormal, _WorldSpaceLightPos0.xyz);
		fixed4 backLight = _BackLightColor * backLightValue;
		
		float3 viewDir = normalize(_WorldSpaceCameraPos.xyz - i.worldPos);
		fixed4 lightSource = LightSource(i.worldPos, i.worldNormal, litTex, viewDir);

		fixed isBack = saturate(max(0, backLightValue * 100));
		isBack *= _BackLightStrength;

		fixed4 newColor = (_Color * (1 -isBack)) + (_BackLightColor * isBack);
		
		col.rgb += lightSource;
		col.rgb *= (newColor + toonLight + backLight) * _LightColor0.rgb;						
		col.a = _Color.a;		
		return col;
	}
		ENDCG
	}
	}
		Fallback "Diffuse"
}