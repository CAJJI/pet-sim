﻿Shader "Toon/Lit Dissolve DoubleTex" {
	Properties{
		_Color("Primary Color", Color) = (1,1,1,1)
		_MainTex("Primary (RGB)", 2D) = "white" {}
	}

		SubShader{
		Tags{ "RenderType" = "Transparent" }

		CGPROGRAM

#pragma surface surf Lambert

		uniform int _LightCount;
	uniform float3 _LightColor[5];
	uniform float3 _LightPosition[5];

	sampler2D _MainTex;
	float4 _Color;


	struct Input {
		float2 uv_MainTex : TEXCOORD0;
		float3 worldPos;
		float3 worldNormal;

	};

	float3 Shading(float3 normal, float3 lightDir, float atten) {
		float NdotL = max(0.0, dot(normalize(normal), normalize(lightDir)));
		//float shading = floor(NdotL * _Threshold) / (_Threshold - 0.5);

		//half diff = NdotL * 0.5 + 0.5;
		//shading *= diff * (atten);

		float shading = NdotL * atten;

		return shading;
	}

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		o.Albedo = c.rgb;		
		o.Alpha = c.a;
	}
	ENDCG
	}
		FallBack "Diffuse"
}


/*
Shader "Custom/EnvironmentShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}		
		_Color("Color", Color) = (1,1,1,1)
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" "LightMode" = "ForwardBase" }

		Pass
		{
			CGPROGRAM			
			#pragma vertex vert
			#pragma fragment frag		
#pragma multi_compile_fwdbase

#include "AutoLight.cginc"
			#include "UnityCG.cginc"

			uniform int _LightCount;
			uniform float3 _LightColor[5];
			uniform float3 _LightPosition[5];

			float3 Shading(float3 normal, float3 lightDir, float atten) {
				float NdotL = max(0.0, dot(normalize(normal), normalize(lightDir)));
				//float shading = floor(NdotL * _Threshold) / (_Threshold - 0.5);

				//half diff = NdotL * 0.5 + 0.5;
				//shading *= diff * (atten);

				float shading = NdotL * atten;

				return shading;
			}

			float3 Lighting(float3 position, float3 normal) {								
				float3 color = (0, 0, 0, 0);				
				for (int i = 0; i < _LightCount; i++) {
					float4 lightPos = UnityObjectToClipPos(_LightPosition[i]);
					float NdotL = max(0.0, dot(normalize(normal), normalize(lightPos - position)));
					//float4 pos = (0, 0, 0, 1);					
					//float3 dist = distance(_LightPosition[i], position);
					float3 dist = distance(lightPos, position);
					//dist = max(0, 5 - dist);
					//float3 sphere = 1 - saturate(dist / 5);
					float3 sphere = max(2,dist);
					color += _LightColor[i] * sphere * NdotL;
			}
				return color;
			}

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 pos : SV_POSITION;
				float3 worldNormal : NORMAL;
				LIGHTING_COORDS(1, 2)
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			v2f vert(appdata_full v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.worldNormal = mul(v.normal.xyz, (float3x3) unity_WorldToObject);
				TRANSFER_VERTEX_TO_FRAGMENT(o);
				return o;
			}

			fixed4 _Color;

			fixed4 frag(v2f i) : SV_Target
			{
				float atten = LIGHT_ATTENUATION(i);
				fixed4 col = tex2D(_MainTex, i.uv);												
				col.rgb *= _Color + Lighting(i.pos	, i.worldNormal) + Shading(i.worldNormal, _WorldSpaceLightPos0, atten);
				return col;
			}
			ENDCG
		}
	}
}
*/