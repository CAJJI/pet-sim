﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileTexture : MonoBehaviour {

    public bool actualScale;
    public Vector2 scaleMultiplier = Vector2.one;
    Vector2 textureScale;
    bool currentActualScale;
    Vector3 currentScale;
    Vector2 currentScaleMultiplier;


	void Start () {
        textureScale = GetComponent<Renderer>().material.mainTextureScale;
        UpdateScale();
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.localScale != currentScale || scaleMultiplier != currentScaleMultiplier || currentActualScale != actualScale)
        {
            UpdateScale();
        }
	}

    public void UpdateScale()
    {
        currentScale = transform.localScale;
        Renderer rend = GetComponent<Renderer>();
        Vector2 scale = Vector3.one;
        Vector2 useScale = Vector3.one;        

        useScale = transform.localScale;
        if (useScale.x > useScale.y)
        {
            scale.x = useScale.x / useScale.y;
            if (actualScale)
                scale *= useScale.y;
        }
        else
        {
            scale.y = useScale.y / useScale.x;
            if (actualScale)
                scale *= useScale.x;
        }

        if (textureScale.x > textureScale.y)
            scale *= textureScale.y;
        else
            scale *= textureScale.x;

        scale /= scaleMultiplier;
        currentScaleMultiplier = scaleMultiplier;
        currentActualScale = actualScale;
        rend.material.SetTextureScale("_MainTex", scale);
    }
}
