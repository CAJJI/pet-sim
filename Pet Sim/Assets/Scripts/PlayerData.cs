﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "PlayerData", menuName = "PetSim/Data/PlayerData")]
public class PlayerData : ScriptableObject {

    public string id;
    public List<PetDefInstance> ownedPets = new List<PetDefInstance>();

}
