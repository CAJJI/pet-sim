﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum NeedType
{
    Hunger,
    Thirst,
    Energy,
    Satisfaction,
}

[System.Serializable]
public class PetNeed
{
    public float value;
    public NeedType type;
}

[System.Serializable]
public class PetNeeds
{
    public PetNeed[] all = new PetNeed[4];
    public PetNeed hunger { get { return all[0]; } }
    public PetNeed thirst { get { return all[1]; } }
    public PetNeed energy { get { return all[2]; } }
    public PetNeed satisfaction { get { return all[3]; } }
}

public enum PersonalityType
{
    Energetic,
    Malice,
    Sociability,
    Dominance,
    Cleanliness,
    Bravery,
    Intelligence,
}

[System.Serializable]
public class PetPersonality
{
    public float value;
    public PersonalityType type;
}

[System.Serializable]
public class PetPersonalities
{
    public PetPersonality[] all = new PetPersonality[7];
    public PetPersonality energetic { get { return all[0]; } }
    public PetPersonality malice { get { return all[1]; } }
    public PetPersonality sociability { get { return all[2]; } }
    public PetPersonality dominance { get { return all[3]; } }
    public PetPersonality cleanliness { get { return all[4]; } }
    public PetPersonality bravery { get { return all[5]; } }
    public PetPersonality intelligence { get { return all[6]; } }
}

public enum PetMood
{
    Neutral,
    Happy,
    Sad,
    Angry,
    Surprised,
    Annoyed,
    Tired,
}

[System.Serializable]
public class PetRelationship
{
    public string id;
    public float friendliness;    
}

public enum PetType
{
    Cat,
}

[System.Serializable]
public class PetDefInstance
{
    public string name;
    public string id;
    public PetType type;
    public PetDef def { get { return Manager.GetPetDef(type); } }
    public Color color;

    public PetNeeds needs = new PetNeeds();
    public PetPersonalities personality = new PetPersonalities();
    public PetMood mood;
    public List<PetRelationship> relationships = new List<PetRelationship>();    

    public void Initialize()
    {
        for (int i = 0; i < needs.all.Length; i++)
        {
            needs.all[i].type = (NeedType)i;
        }

        for (int i = 0; i < personality.all.Length; i++)
        {
            personality.all[i].type = (PersonalityType)i;
        }
    }

    public PetNeed GetNeed(NeedType needType)
    {
        foreach(PetNeed need in needs.all)
        {
            if (need.type == needType)
                return need;
        }
        return null;
    }

    public PetPersonality GetPersonality(PersonalityType personalityType)
    {
        foreach (PetPersonality personality in personality.all)
        {
            if (personality.type == personalityType)
                return personality;
        }
        return null;
    }
}

[System.Serializable]
public class PetDef {

    public PetType type;
    public GameObject prefab;
    public string[] discludeMaterial;

    public float[] movementSpeed = new float[3];

    public bool IncludeMaterial(Renderer renderer)
    {
        foreach (string name in discludeMaterial)
        {
            if (name == renderer.gameObject.name) return false;
        }
        return true;
    }
}
