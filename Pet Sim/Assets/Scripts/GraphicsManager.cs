﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GraphicsManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        UpdateLights();
    }

    void UpdateLights()
    {
        Shader.SetGlobalVectorArray("globalLightPosition", new Vector4[20]);
        Shader.SetGlobalVectorArray("globalLightColor", new Vector4[20]);
        Shader.SetGlobalFloatArray("globalLightIntensity", new float[20]);
        Shader.SetGlobalFloatArray("globalLightRadius", new float[20]);
        Shader.SetGlobalFloatArray("globalLightDampen", new float[20]);        

        LightSource[] lights = FindObjectsOfType<LightSource>();
        Vector4[] positions = new Vector4[20];
        Vector4[] colors = new Vector4[20];
        float[] intensities = new float[20];
        float[] radiuses = new float[20];
        float[] dampening = new float[20];
        for (int i = 0; i < lights.Length; i++)
        {
            positions[i] = lights[i].transform.position;                      
            colors[i] = new Vector4(lights[i].color.r, lights[i].color.g, lights[i].color.b, lights[i].color.a);
            intensities[i] = lights[i].intensity;
            radiuses[i] = lights[i].radius;
            dampening[i] = lights[i].dampening;
        }

        int lightCount = lights.Length;
        Shader.SetGlobalInt("globalLightCount", lightCount);
        if (lightCount > 0)
        {
            Shader.SetGlobalVectorArray("globalLightPosition", positions);
            Shader.SetGlobalVectorArray("globalLightColor", colors);
            Shader.SetGlobalFloatArray("globalLightIntensity", intensities);
            Shader.SetGlobalFloatArray("globalLightRadius", radiuses);
            Shader.SetGlobalFloatArray("globalLightDampen", dampening);
        }        
    }
}
