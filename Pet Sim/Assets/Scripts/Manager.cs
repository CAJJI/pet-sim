﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Manager))]
[CanEditMultipleObjects]
public class ManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Manager manager = (Manager)target;

        if (GUILayout.Button("Update Room"))
        {
            manager.gameDef.UpdateRoom(manager.room);
        }
    }
}

public class Manager : Singleton<Manager> {

    public PetDefInstance instance;

    public GameDef gameDef;
    public GameObject room;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            CreatePet(instance, Vector3.zero);
        }
    }

    public string GenerateId()
    {
        string id = "";
        char[] alphabet = "abcdefghijklmnopqrstuvwxyz".ToCharArray();        
        for (int i = 0; i < 10; i++)
        {
            int value = Random.Range(0, alphabet.Length);
            id += alphabet[value];
        }
        return id;
    }

    public static PetDef GetPetDef(PetType type)
    {
        foreach(PetDef def in Instance.gameDef.petData.defs)
        {
            if (def.type == type)
                return def;
        }
        return null;
    }

    public static void CreatePet(PetDefInstance instance, Vector3 position)
    {
        GameObject prefab = instance.def.prefab;        
        GameObject prefabInstance = Instantiate(prefab, position, Quaternion.identity);
        ApplyInstance(prefabInstance, instance);
    }

    public static void ApplyInstance(GameObject prefabInstance, PetDefInstance instance)
    {
        Pet pet = prefabInstance.GetComponent<Pet>();
        pet.instance = instance;        
        foreach(Renderer rend in pet.GetRenderers())
        {
            rend.material.color = instance.color;
        }
    }
}
