﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CreateAssetMenu(fileName = "PetData", menuName ="PetSim/Data/PetData")]
public class PetData : ScriptableObject {

    public PetDef[] defs;    
}
