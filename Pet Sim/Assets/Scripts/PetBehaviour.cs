﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PetBehaviour : PetSequenceBehaviour {


    public bool CheckNeeds()
    {
        if (pet.instance.needs.hunger.value < 50)
        {
            if (GetFood())
                return true;
        }
        if (pet.instance.needs.energy.value < 50)
        {            
            if (GetBed())
                return true;
        }
        return false;
    }

    public bool CheckMood()
    {
        return false;
    }

    public bool GetFood()
    {
        NeedSatisfier foodSource = GetNearestNeedSatisfier(pet.transform.position, NeedType.Hunger, pet.instance.type);
        if (foodSource && foodSource.canUse)
        {
            StartSequence(GetFood(foodSource));
            return true;
        }
        return false;
    }

    public bool GetBed()
    {
        NeedSatisfier bed = GetNearestNeedSatisfier(pet.transform.position, NeedType.Energy, pet.instance.type);
        Debug.Log(bed && bed.canUse);
        if (bed && bed.canUse)
        {            
            StartSequence(GoToBed(bed));
            return true;
        }
        return false;
    }

    public NeedSatisfier GetNearestNeedSatisfier(Vector3 position, NeedType needType, PetType petType)
    {        
        List<NeedSatisfier> qualifiers = new List<NeedSatisfier>();
        foreach(NeedSatisfier needSatisfier in GameObject.FindObjectsOfType<NeedSatisfier>())
        {
            if (needSatisfier.needTypes.Contains(needType) && needSatisfier.petTypes.Contains(petType)){
                qualifiers.Add(needSatisfier);
            }
        }
        return GetNearest<NeedSatisfier>(position, qualifiers);
    }    

    public T GetNearest<T>(Vector3 position, List<T> list) where T: MonoBehaviour
    {        
        T nearest = null;
        float distance = 1000;

        for (int i = 0; i < list.Count; i++)
        {
            float newDistance = (position - list[i].transform.position).magnitude;
            if (newDistance < distance)
            {
                distance = newDistance;
                nearest = list[i];
            }
        }
        return nearest;
    }
}
