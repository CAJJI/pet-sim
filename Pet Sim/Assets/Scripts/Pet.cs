﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PetSpeedType
{
    Slow, Normal, Fast
}

public class Pet : MonoBehaviour {

    public PetDefInstance instance;
    public PetBehaviour behaviour = new PetBehaviour();
    public PetAnimator animator = new PetAnimator();

    void Start()
    {
        instance.Initialize();
        animator.Initialize(this);
        behaviour.Initialize(this);
    }

    public Renderer[] GetRenderers()
    {
        List<Renderer> renderers = new List<Renderer>();
        foreach (Renderer rend in GetComponentsInChildren<Renderer>())
        {
            if (instance.def.IncludeMaterial(rend))
                renderers.Add(rend);
        }
        return renderers.ToArray();
    }

    private void Update()
    {
        animator.Update();

        if (behaviour.PlaySequence()) return;
        else
        {
            if (behaviour.CheckNeeds()) return;
        }

        //if in action, continue
        
        //else check what to do
        
        //check mood        
        //check personality        
        //check surroundings
    }

    public void SendEvent(string message)
    {
        Debug.Log(message);
        animator.SendEvent(message);
    }

    public void AdjustNeed(NeedType needType, float value)
    {
        PetNeed need = instance.GetNeed(needType);
        need.value = Mathf.Clamp(need.value + value, 0, 100);
    }

    public void Move(Vector3 direction, PetSpeedType speedType)
    {
        LookAt(transform.position + direction);
        float speed = instance.def.movementSpeed[(int)speedType];        
        direction.y = 0;
        transform.position += direction.normalized * speed;

        animator.movement.y = 1 + (int)speedType;

        animator.movementSpeed.value = speed * 200;        
    }

    public bool LookAt(Vector3 position)
    {
        Vector3 direction = position - transform.position;
        direction.y = 0;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(direction), 0.03f);
        
        Vector3 localDirection = Quaternion.Inverse(transform.rotation) * -direction;;
        animator.movement.y /= -localDirection.x;
        animator.movement.x = -localDirection.x;
        return (direction.normalized - transform.forward).magnitude < 0.1f;
    }
}
