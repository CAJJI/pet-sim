﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameDef))]
[CanEditMultipleObjects]
public class GameDefEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GameDef gameDef = (GameDef)target;

    }
}

[CreateAssetMenu(fileName = "GameDef", menuName = "PetSim/GameDef")]
public class GameDef : ScriptableObject
{

    public PetData petData;
    public Material wallpaper;
    public Material baseboard;
    public Material ceiling;
    public Material carpet;

    public void UpdateRoom(GameObject room)
    {
        UpdateMaterial(room, "Ceiling", ceiling);
        UpdateMaterial(room, "Baseboard", baseboard);
        UpdateMaterial(room, "LBaseboard", baseboard);
        UpdateMaterial(room, "RBaseboard", baseboard);
        UpdateMaterial(room, "Carpet", carpet);
        UpdateMaterial(room, "Back", wallpaper);
        UpdateMaterial(room, "LWall", wallpaper);
        UpdateMaterial(room, "RWall", wallpaper);
    }

    public void UpdateMaterial(GameObject GO, string childName, Material material)
    {
        if (material)
            GO.transform.Find(childName).GetComponent<Renderer>().material = material;
    }
}