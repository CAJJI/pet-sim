﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightSource : MonoBehaviour {

    public enum LightType
    {
        Point, Spot
    }

    [Range(0,10)]
    public float dampening = 0;
    [Range(0, 100)]
    public float intensity = 1;
    [Range(0, 100)]
    public float radius = 1;
    public Color color = new Color(1,1,1,1);

	void Update()
    {        
    }
}
