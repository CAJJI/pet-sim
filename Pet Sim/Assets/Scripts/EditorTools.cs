﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class EditorTools {

    [MenuItem("Tools/Selection/Snap Position %x")]
    static void SnapPosition()
    {
        SnapPosition(true);
    }

    static void SnapPosition(bool registerUndo)
    {
        if (registerUndo)
            Undo.RegisterCompleteObjectUndo(Selection.activeTransform, "Snap Position");
        Selection.activeTransform.position = RoundPosition(Selection.activeTransform.position);
    }

    public static Vector3 RoundPosition(Vector3 position)
    {
        Vector3 newPos = new Vector3();
        newPos.x = Mathf.Round(position.x);
        newPos.y = Mathf.Round(position.y);
        newPos.z = Mathf.Round(position.z);
        return newPos;
    }
}
