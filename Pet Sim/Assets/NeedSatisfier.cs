﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedSatisfier : Furniture {
    
    public List<NeedType> needTypes;
    public List<PetType> petTypes;
    public float engageDistance;
    public float value = 1;
    public int amountRemaining = 1;
    public bool infiniteAmount;
    public bool canUse { get { return amountRemaining > 0; } }

    public void AdjustAmount(int value)
    {
        if (infiniteAmount) return;
        amountRemaining = Mathf.Clamp(amountRemaining + value, 0, 100);
    }
}
