﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetSequenceStep
{
    public static PetAction.MoveToPositionStep MoveToPosition(Pet pet, Vector3 position, float engageDistance, PetSpeedType speedType)
    {
        return new PetAction.MoveToPositionStep() { pet = pet, position = position, engageDistance = engageDistance, speedType = speedType};
    }

    public static PetAction.EatFromFoodSource EatFromFoodSource(Pet pet, NeedSatisfier foodSource)
    {
        return new PetAction.EatFromFoodSource(){ pet = pet, foodSource = foodSource};
    }

    public static PetAction.FacePosition FacePosition(Pet pet, Vector3 position)
    {
        return new PetAction.FacePosition() { pet = pet, position = position };
    }

    public static PetAction.PlayAnimation PlayAnimation(Pet pet, PetAnimationType animationType)
    {
        return new PetAction.PlayAnimation() { pet = pet, animationType = animationType };
    }

    public static PetAction.WaitForAnimationEvent WaitForAnimationEvent(Pet pet, PetAnimationEventType eventType)
    {
        return new PetAction.WaitForAnimationEvent() { pet = pet, eventType = eventType };
    }
}

public class PetAction
{
    public bool complete;
    public Pet pet;

    public virtual void Play()
    {

    }

    public class MoveToPositionStep : PetAction
    {     
        public Vector3 position;
        public float engageDistance;
        public PetSpeedType speedType;

        public override void Play()
        {
            Vector3 direction = position - pet.transform.position;
            float distance = direction.magnitude;
            if (distance > engageDistance + 0.1f)
                pet.Move(direction, speedType);
            else
                complete = true;
        }
    }

    public class EatFromFoodSource : PetAction
    {     
        public NeedSatisfier foodSource;

        public override void Play()
        {
            if (!foodSource.canUse)
            {
                complete = true;
                return;
            }

            if (pet.instance.needs.hunger.value < 100)
            {                
                if (pet.animator.GetEvent(PetAnimationEventType.Eat).isActive)
                {                    
                    pet.AdjustNeed(NeedType.Hunger, foodSource.value);
                    foodSource.AdjustAmount(-1);
                }
            }
            else
            {
                pet.animator.PlayAnimation(PetAnimationType.StopEat);
                complete = true;
            }
        }
    }

    public class FacePosition : PetAction
    {        
        public Vector3 position;

        public override void Play()
        {
            if (pet.LookAt(position))
                complete = true;
        }
    }

    public class PlayAnimation : PetAction
    {        
        public PetAnimationType animationType;

        public override void Play()
        {
            pet.animator.PlayAnimation(animationType);
            complete = true;
        }
    }

    public class WaitForAnimationEvent : PetAction
    {        
        public PetAnimationEventType eventType;

        public override void Play()
        {
            if (pet.animator.GetEvent(eventType).isActive)
                complete = true;
        }
    }
}
