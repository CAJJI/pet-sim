﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetBool
{
    public bool value;
    int delay;

    public void Update()
    {
        if (value)
        {
            delay++;
            if (delay > 1)
            {
                value = false;
                delay = 0;
            }            
        }
    }

    public static implicit operator bool(ResetBool resetBool)
    {
        return resetBool.value;
    }
}

public enum PetAnimationEventType
{
    Eat,
    Start,
    Finish,
    Length,
}

public enum PetAnimationType
{
    Walk,
    Eat,
    StopEat,
    Sit,
    Stand,
    Jump,
    Fall,
    Sleep,
    WakeUp,
    Wave
}

public class PetAnimationEvent
{
    public PetAnimationEventType type;
    public string message;
    public ResetBool isActive = new ResetBool();
}

public class PetAnimator {

    public Pet pet;
    public Animator animator;

    public Vector2 movementFollow;
    public Vector2 movement;
    AnimatorFloat movementX;
    AnimatorFloat movementY;
    public AnimatorFloat movementSpeed;
    public List<AnimatorVariable> animatorVariables = new List<AnimatorVariable>();

    public List<PetAnimationEvent> animationEvents = new List<PetAnimationEvent>();    

    public void Initialize(Pet pet)
    {
        this.pet = pet;
        animator = pet.GetComponent<Animator>();

        for (int i = 0; i < (int)PetAnimationEventType.Length; i++)
        {
            animationEvents.Add(new PetAnimationEvent()
            {
                type = (PetAnimationEventType)i,
                message = System.Enum.GetName(typeof(PetAnimationEventType), i)
            });
        }

        AssignAnimationVariables();
    }

    void AssignAnimationVariables()
    {
        animatorVariables.Add(movementX = new AnimatorFloat(animator, "movementX"));
        animatorVariables.Add(movementY = new AnimatorFloat(animator, "movementY"));
        animatorVariables.Add(movementSpeed = new AnimatorFloat(animator, "movementSpeed"));
    }

    public void Update()
    {
        foreach(PetAnimationEvent animEvent in animationEvents)
        {
            animEvent.isActive.Update();
        }

        foreach(AnimatorVariable animVar in animatorVariables)
        {
            animVar.Update();
        }

        movementSpeed.value = Mathf.Lerp(movementSpeed.value, 1, 0.1f);

        movementFollow = Vector2.Lerp(movementFollow, movement, 0.1f);
        movementX.value = movementFollow.x;
        movementY.value = movementFollow.y;
        movement = Vector2.Lerp(movement, Vector3.zero, 0.1f);
    }

    public void PlayAnimation(PetAnimationType animationType)
    {
        animator.SetTrigger(GetAnimationToString(animationType));
    }

    public void SendEvent(string sendMessage)
    {
        foreach(PetAnimationEvent animEvent in animationEvents)
        {
            if (animEvent.message == sendMessage)
                animEvent.isActive.value = true;
        }
    }	

    public PetAnimationEvent GetEvent(PetAnimationEventType eventType)
    {
        foreach (PetAnimationEvent animEvent in animationEvents)
        {
            if (animEvent.type == eventType)
                return animEvent;
        }
        return null;
    }
    
    public string GetAnimationToString(PetAnimationType animationType)
    {
        return System.Enum.GetName(typeof(PetAnimationType), animationType);
    }
}
