﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;

[CustomEditor(typeof(AnimatorTool))]
[CanEditMultipleObjects]
public class AnimatorToolEditor : EditorTool
{    
    public override void OnInspectorGUI()
    {
        AnimatorTool tool = (AnimatorTool)target;

        BeginHorizontal(() =>
        {
            GUILayout.Label("Function Name", GUILayout.Width(100));
            tool.newEvent.functionName = EditorGUILayout.TextField(tool.newEvent.functionName);
        });

        BeginHorizontal(() =>
        {
            GUILayout.Label("Float", GUILayout.Width(100));
            tool.newEvent.floatParameter = EditorGUILayout.FloatField(tool.newEvent.floatParameter);
        });
        BeginHorizontal(() =>
        {
            GUILayout.Label("Int", GUILayout.Width(100));
            tool.newEvent.intParameter = EditorGUILayout.IntField(tool.newEvent.intParameter);
        });
        BeginHorizontal(() =>
            {
                GUILayout.Label("String", GUILayout.Width(100));
                tool.newEvent.stringParameter = EditorGUILayout.TextField(tool.newEvent.stringParameter);
            });

        DrawDefaultInspector();

        if (GUILayout.Button("Add To Clips"))
        {
            tool.AddToClips();
        }

        if (GUILayout.Button("Remove From Clips"))
        {
            tool.RemoveFromClips();
        }

        BeginHorizontal(() =>
        {
            GUILayout.Label("Parameter Name", GUILayout.Width(100));
            tool.parameter.name = EditorGUILayout.TextField(tool.parameter.name);
        });

        BeginHorizontal(() =>
        {
            GUILayout.Label("Parameter Type", GUILayout.Width(100));
            tool.parameter.type = (AnimatorControllerParameterType)EditorGUILayout.EnumPopup("", tool.parameter.type);
        });

        if (GUILayout.Button("Add Parameter"))
        {
            tool.AddParameters();
        }

        if (GUILayout.Button("Remove Parameter"))
        {
            tool.RemoveParameters();
        }
    }
}

[CreateAssetMenu(fileName = "Animator Tool", menuName = "PetSim/Tools/AnimatorTool")]
public class AnimatorTool : ScriptableObject {

    public AnimationClip[] clips;
    public AnimationEvent newEvent;
    public float time;
    public bool useLength;
    [Range(0, 1)]
    public float length;

    public void AddToClips()
    {
        Undo.RegisterCompleteObjectUndo(clips, "AddToClips");
        foreach (AnimationClip clip in clips)
        {
            float time = this.time;
            if (time > clip.length) time = clip.length;

            if (useLength)
            {
                time = clip.length * length;
            }

            newEvent.time = time;

            List<AnimationEvent> eventList = new List<AnimationEvent>();
            foreach (AnimationEvent animEvent in clip.events) {
                eventList.Add(animEvent);
                if (animEvent.functionName == newEvent.functionName && animEvent.time == newEvent.time) return;
            }
            eventList.Add(newEvent);

            AnimationUtility.SetAnimationEvents(clip, eventList.ToArray());


        }
    }

    public void RemoveFromClips()
    {
        Undo.RegisterCompleteObjectUndo(clips, "RemoveFromClips");
        foreach (AnimationClip clip in clips)
        {
            List<AnimationEvent> eventList = new List<AnimationEvent>();
            foreach (AnimationEvent animEvent in clip.events)
            {
                if (animEvent.functionName != newEvent.functionName)
                    eventList.Add(animEvent);
            }

            AnimationUtility.SetAnimationEvents(clip, eventList.ToArray());


        }
    }


    public AnimatorController[] animators;
    public AnimatorControllerParameter parameter = new AnimatorControllerParameter();
    public bool addAllAnimationTypes;
    
    public void AddParameters()
    {
        if (addAllAnimationTypes)
        {
            string[] names = System.Enum.GetNames(typeof(PetAnimationType));
            foreach (string name in names)
            {
                foreach (AnimatorController animator in animators)
                {
                    bool skip = false;
                    for (int i = 0; i < animator.parameters.Length; i++)
                    {
                        if (animator.parameters[i].name == name)
                        {
                            animator.RemoveParameter(i);
                            //skip = true;
                        }
                    }
                    //if (!skip)
                    animator.AddParameter(name, AnimatorControllerParameterType.Trigger);
                }
            }
        }

        else
        {
            foreach (AnimatorController animator in animators)
            {
                animator.AddParameter(parameter);
            }
        }        
    }

    public void RemoveParameters()
    {        
        foreach (AnimatorController animator in animators)
        {                        
            for(int i = 0; i < animator.parameters.Length; i++)
            {
                if (animator.parameters[i].name == parameter.name)
                {
                    animator.RemoveParameter(i);                    
                }                
            }
        }
    }
}
