﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : Item {

	public virtual void PlayerInteract()
    {

    }

    public virtual void PetInteract(Pet pet)
    {

    }
}
