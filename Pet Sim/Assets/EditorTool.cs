﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

public class EditorTool : Editor
{    

    public void BeginVertical(Action action, GUIStyle style = null)
    {
        GUILayout.BeginVertical(EditorStyles.helpBox);
        action();
        GUILayout.EndHorizontal();
    }

    public void BeginHorizontal(Action action, GUIStyle style = null)
    {
        if (style == null) style = EditorStyles.helpBox;
        GUILayout.BeginHorizontal(style);
        action();
        GUILayout.EndHorizontal();
    }

    public bool CreateButton(string label, Color color, float width = 0)
    {
        GUI.backgroundColor = color;
        bool state = false;
        if (width > 0)
            state = GUILayout.Button(label, GUILayout.Width(width));
        else
            state = GUILayout.Button(label);

        GUI.backgroundColor = Color.white;
        return state;
    }

    public string GetOptionByNumber(int number, List<string> list)
    {
        if (number < 0 || number >= list.Count) return "";
        return list[number];
    }

    public int GetOptionByName(string name, string[] options)
    {
        for (int i = 0; i < options.Length; i++)
        {
            if (name == options[i])
                return i;
        }
        return 1;
    }
}
